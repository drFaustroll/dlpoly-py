'''
Module to handle MSDTMP config files.
'''

from typing import Optional, Any, List
import numpy as np

from .types import OptPath, PathLike


class MSD:
    """
    Class relating to MSD data.

    Attributes
    ----------
    source : OptPath
        File data originally read from.
    title : str
        DLPoly comment string.
    n_frames : int
        Number of frames in MSD.
    n_atoms : int
        Number of atoms in MSD.
    latom : List[List[str]]
        List of atoms in MSD.
    timestep : float
        Timestep of MSD.
    data : Optional[np.typing.NDArray[np.float64]]
        MSD data columns.
    step : Optional[np.typing.NDArray[np.float64]]
        Timesteps.
    time : Optional[np.typing.NDArray[np.float64]]
        Time of sample.
    species : Optional[np.typing.NDArray[Any]]
        Species enumeration.
    """
    def __init__(self, source: OptPath = None):
        """
        Instantiate class relating to MSD data.

        Parameters
        ----------
        source : OptPath
            File to read.
        """
        self.n_frames = 0
        self.n_atoms = 0
        self.latom: List[List[str]] = []
        self.timestep = 0.
        self.data: Optional[np.typing.NDArray[np.float64]] = None
        self.step: Optional[np.typing.NDArray[np.float64]] = None
        self.time: Optional[np.typing.NDArray[np.float64]] = None
        self.title = ""
        self.species: Optional[np.typing.NDArray[Any]] = None

        if source is not None:
            self.source = source
            self.read(source)

    @property
    def n_species(self) -> int:
        """
        Number of species in MSD.
        """
        return len(self.species)

    def per_species(self) -> np.ndarray:
        """
        List by species.

        Returns
        -------
        np.ndarray
            List of species averages.
        """
        data = np.zeros((self.n_frames, self.n_species, 2))
        for i in range(self.n_frames):
            for j, species in enumerate(self.species):
                m = [x == species for x in self.latom[i]]
                data[i, j, 0] = np.average(self.data[i, m, 0])
                data[i, j, 1] = np.average(self.data[i, m, 1])

        return data

    def read(self, filename: PathLike = "MSDTMP") -> "MSD":
        """
        Read an MSDTMP file.

        Parameters
        ----------
        filename : PathLike
            File to read.

        Returns
        -------
        MSD
            Constructed MSD.
        """

        with open(filename, "r", encoding="utf-8") as in_file:
            data_in = map(lambda x: x.strip().split(), iter(in_file))

            self.title = next(data_in)[0]
            self.n_atoms, self.n_frames, _ = map(int, next(data_in))

            self.data = np.zeros((self.n_frames, self.n_atoms, 2))
            self.step = np.zeros(self.n_frames)
            self.time = np.zeros(self.n_frames)

            for i in range(self.n_frames):
                header = next(data_in)
                self.step[i] = int(header[1])
                self.timestep = float(header[3])
                self.time[i] = float(header[4])
                frame_species = []
                for j in range(self.n_atoms):
                    species, _, mean_sq, t = next(data_in)
                    self.data[i, j, :] = float(mean_sq)**2, float(t)
                    frame_species.append(species)
                self.latom.append(np.sort(frame_species))
        # nb now np.sort(np.unique()), as set() can have non-deterministic behaviour on
        #   non deterministic input (an issue for unit tests)
        #   https://stackoverflow.com/questions/51927850/is-set-deterministic
        self.species = np.sort(np.unique(self.latom[0]))

        return self
