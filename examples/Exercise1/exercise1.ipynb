{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Anisotropic constant pressure MD¶\n",
    "\n",
    "## Summary\n",
    "\n",
    "This exercise studies a well-known phase transition in potassium chloride, see ref. (Parrinello and A. Rahman, Polymorphic transitions in alkali halides. A molecular dynamics study, Journal de Physique Colloques, 42 C6, p. C6, 1981, doi: 10.1051/jphyscol:19816149, URL https://hal.archives-ouvertes.fr/jpa-00221214.), using constant pressure molecular dynamics. The objective is to develop the best practice in using such algorithms and to learn how phase transitions can be induced, detected and monitored in a simulation.\n",
    "\n",
    "## Background\n",
    "\n",
    "Potassium chloride at ambient temperature and pressure adopts the cubic rocksalt structure, in which each ion is surrounded by six ions of opposite charge in an octahedral arrangement. Under high pressure this structure transforms to something more close packed - the so-called caesium chloride structure, where the nearest neighbour coordination rises to eight ions. (Using the model potential adopted here, this occurs at about 1.4 GPa.) In this exercise the student will have the opportunity to see this phase transition using the method of anisotropic constant pressure molecular dynamics. Commencing with the rocksalt crystal structure and applying a fixed external pressure it is possible to induce the phase transition in a simulation. Similarly it is possible to see the reverse transition back to rocksalt. However it is not necessarily trivial to make these transitions happen in any given simulation (though you may be lucky the first time!) Your task will be to find the conditions under which the phase transition occurs. This will not be entirely a matter of finding the right conditions of temperature and pressure, but will also involve setting up the control parameters for the simulation so as to encourage the phase transition to occur. (Even if the transformation is thermodynamically permitted, it does not follow that it will happen in the lifetime of a simulation.)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "Be sure you have setup your DLPOLY environment by executing the appropiate setup environment. eg\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "git clone https://gitlab.com/ccp5/dl-poly.git\n",
    "\n",
    "cmake -S dl-poly -Bbuild-dlpoly -DCMAKE_BUILD_TYPE=Release\n",
    "cmake --build build-dlpoly\n",
    "cmake --install build-dlpoly\n",
    "\n",
    "python3 -m pip install dlpoly-py matplotlib py3Dmol ipywidgets git+https://gitlab.com/ase/ase.git"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will need the input files for DLPOLY, a control file, a FIELD file and a CONFIG file. The last of these is a crystal of potassium chloride at ambient temperature and pressure (i.e. in the rocksalt structure). You should proceed as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "\n",
    "mkdir -p Exercise1\n",
    "cd Exercise1\n",
    "wget https://gitlab.com/drFaustroll/dlpoly-py/-/raw/devel/examples/data/ex1/CONFIG\n",
    "wget https://gitlab.com/drFaustroll/dlpoly-py/-/raw/devel/examples/data/ex1/FIELD\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "now we will create the control files in this case is called ex1.control and saved in folder Exercise1/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile Exercise1/ex1.control\n",
    "title DL_POLY: Potassium Chloride Test Case \n",
    "\n",
    "temperature  300.0 K\n",
    "print_frequency  10 steps\n",
    "stats_frequency  10 steps\n",
    "rdf_print ON\n",
    "rdf_calculate ON\n",
    "rdf_frequency  10 steps\n",
    "vdw_cutoff  7.0 ang\n",
    "padding  0.25 ang\n",
    "cutoff  7.0 ang\n",
    "coul_method ewald\n",
    "ewald_precision 1e-06\n",
    "ensemble nst\n",
    "ensemble_method hoover\n",
    "ensemble_thermostat_coupling  0.1 ps\n",
    "ensemble_barostat_coupling  1.0 ps\n",
    "time_run  2000 steps\n",
    "time_equilibration  500 steps\n",
    "time_job  3600.0 s\n",
    "time_close  100.0 s\n",
    "timestep  0.001 ps\n",
    "pressure_hydrostatic  40.0 katm\n",
    "restart continue\n",
    "rescale_frequency  10 steps\n",
    "traj_calculate ON\n",
    "traj_start  500 steps\n",
    "traj_interval  100 steps\n",
    "traj_key pos-vel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "also we will add soem helper function showrdf to allow us to visualise the rdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dlpoly import DLPoly\n",
    "from dlpoly.rdf import RDF\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def showrdf(loc):\n",
    "    m = RDF(loc)\n",
    "    for i in range(len(m.labels)):\n",
    "        plt.plot(m.x, m.data[i,:,0],label = \"-\".join(m.labels[i]))\n",
    "    plt.xlabel(\"r [Å])\")\n",
    "    plt.ylabel(\"gofr [a.u.])\")\n",
    "    plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "load the control, field and config and setup a working directory called w40 in this case.\n",
    "second line is running dlpoly for you and the third line will show your the rdf.\n",
    "Last three lines indicate at what pressure and temperature the simulation is run (check the manual for the units) and the statistical ensemble we use to sample."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dlPoly = DLPoly(control=\"Exercise1/ex1.control\", config=\"Exercise1/CONFIG\",\n",
    "                field=\"Exercise1/FIELD\", workdir=\"w40\")\n",
    "dlPoly.run(numProcs = 1)\n",
    "showrdf(\"w40/RDFDAT\")\n",
    "print(\"Pressure: {} unit\".format(*dlPoly.control.pressure_hydrostatic))\n",
    "print(\"Temperature: {} unit\".format(dlPoly.control.temperature))\n",
    "print(dlPoly.control.ensemble)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "now we will visualize REVCON the last frame of your simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.io import read,write\n",
    "import py3Dmol\n",
    "\n",
    "styles = ['line', 'stick', 'sphere']\n",
    "\n",
    "def atomsView(a, size=(300, 300), style=\"stick\",background='0xfeeeee'):\n",
    "\n",
    "    assert style in styles\n",
    "\n",
    "    write(\"temp.cif\",a)\n",
    "    c = open(\"temp.cif\",'r').read()\n",
    "    view = py3Dmol.view()\n",
    "    view.addModel(c,'cif')\n",
    "    view.setStyle({style:{}})\n",
    "    view.setBackgroundColor(background)\n",
    "    view.zoomTo()\n",
    "    view.addUnitCell()\n",
    "\n",
    "    return view\n",
    "\n",
    "a=read(\"w40/REVCON\",format=\"dlp4\")\n",
    "a.wrap()\n",
    "\n",
    "atomsView(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "now inspect the text output of your simulation, (replace cat with your favourite graphical text editor if you run on a local jupyter server)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!cat OUTPUT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat the simulation at a different state point, where you might expect a phase transition to occur. Examine the result graphically once again (using the REVCON file) and try to deduce how the phase transition occurred. Look at the RDF plots and try to determine what phase the structure is now in.\n",
    "\n",
    "as previously you load the files and setup a new working directory.\n",
    "change the pressure and temperature to the desired values (60 for pressure, 500 for temperature) and rerun"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "dlPoly = DLPoly(control=\"Exercise1/ex1.control\", config=\"Exercise1/CONFIG\",\n",
    "                field=\"Exercise1/FIELD\", workdir=\"w60\")\n",
    "dlPoly.control.pressure_hydrostatic = (60, \"katm\")\n",
    "dlPoly.control.temperature = (500, \"K\")\n",
    "dlPoly.run(numProcs = 1)\n",
    "showrdf(\"w60/RDFDAT\")\n",
    "a=read(\"w60/REVCON\",format=\"dlp4\")\n",
    "a.wrap()\n",
    "\n",
    "atomsView(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you do not see a phase transition, experiment with the control parameters (e.g. change the relaxation times, temperature or pressure, as you think fit) until you see one. Be as systematic as you can, using whatever insight you gain to rationalise what’s going on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dlPoly = DLPoly(control=\"Exercise1/ex1.control\", config=\"Exercise1/CONFIG\",\n",
    "                field=\"Exercise1/FIELD\", workdir=\"w50\")\n",
    "dlPoly.control.pressure_hydrostatic = (60, \"katm\")\n",
    "dlPoly.control.temperature = (500, \"K\")\n",
    "dlPoly.control.ensemble_thermostat_coupling =(0.1, \"ps\")\n",
    "dlPoly.control.ensemble_barostat_coupling = (1.0, \"ps\")\n",
    "dlPoly.run(numProcs = 1)\n",
    "showrdf(\"w50/RDFDAT\")\n",
    "a=read(\"w50/REVCON\",format=\"dlp4\")\n",
    "a.wrap()\n",
    "\n",
    "atomsView(a)"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "5ec3d3b526b29dc3bba0cd884d6534eac5478ee0512311bb0b11199bd680d356"
  },
  "kernelspec": {
   "display_name": "Python 3.9.5 64-bit ('dlpoly': venv)",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
